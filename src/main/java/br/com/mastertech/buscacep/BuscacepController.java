package br.com.mastertech.buscacep;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BuscacepController {

    @Autowired
    BuscacepFeign buscacepFeign;

    @GetMapping("buscacep/{cep}")
    public EnderecoDTO receber(@PathVariable String cep) {
        EnderecoDTO enderecoDTO = buscacepFeign.enderecoDTO(cep);
        return enderecoDTO;
    }
}

