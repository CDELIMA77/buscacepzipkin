package br.com.mastertech.buscacep;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cep", url = "https://viacep.com.br/ws/")
public interface BuscacepFeign {
    @GetMapping("/{cep}/json")
    EnderecoDTO enderecoDTO(@PathVariable String cep);
}
